package com.yj.dynamicProxyPattern;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class NonOwnerInvocationHandler implements InvocationHandler{
	PersonBean personBean;
	
	public NonOwnerInvocationHandler(PersonBean personBean) {
		super();
		this.personBean = personBean;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if(method.getName().startsWith("setHotOrNot")){
			return method.invoke(personBean, args);
		}else if(method.getName().startsWith("set")){
			throw new IllegalAccessException();
		}else if(method.getName().startsWith("get")){
			return method.invoke(personBean, args);
		}
		return null;
	}

}
