package com.yj.TemplateMethodPattern;

public class Tea extends CaffeineBeverage{
	
	
	public void boilWater(){
		System.out.println("Boiling water");
	}
	
	public void steepTeaBag(){
		System.out.println("Steeping the tea");
	}
	
	public void addLemon(){
		System.out.println("Adding Lemon");
	}
	
	public void pourInCup(){
		System.out.println("Pouring into cup");
	}

	@Override
	void brew() {
		System.out.println("Steeping the tea");
	}

	@Override
	void addCondiments() {
		System.out.println("Adding Lemon");
	}
}
