package com.yj.TemplateMethodPattern;

public abstract class AbstractClass {
	
	final void templateMethod(){
		primitiveOperation1();
		primitiveOperation2();
		concreteOperation();
	}
	
	abstract void primitiveOperation1();
	
	abstract void primitiveOperation2();
	
	final void concreteOperation(){}
	
	void hook(){};
}
