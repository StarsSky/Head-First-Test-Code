package com.yj.TemplateMethodPattern;

public class BrewTea {
	
	public static void main(String[] args) {
		Tea tea=new Tea();
		tea.prepareRecipe();
	}
}
