package com.yj.TemplateMethodPattern;

public class Coffee extends CaffeineBeverage{
	
	
	public void boilWater(){
		System.out.println("Boiling water");
	}
	
	public void brewCoffeeGrinds() {
		System.out.println("Dripping Coffee through filter");
	}
	
	public void pourInCup(){
		System.out.println("Pouring into cup");
	}
	
	public void addSugarAndMilk(){
		System.out.println("Adding Sugar and Milk");
	}

	@Override
	void brew() {
		System.out.println("Dripping Coffee through filter");
	}

	@Override
	void addCondiments() {
		System.out.println("Adding Sugar and Milk");
	}
}
