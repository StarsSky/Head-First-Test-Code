package com.yj.IteratorAndCompositePattern;

import java.util.ArrayList;

public class PancakeHouseIterator implements Iterator{
	ArrayList items;
	int position=0;
	
	public PancakeHouseIterator(ArrayList items) {
		this.items=items;
	}
	
	@Override
	public boolean hasNext() {
		if(position>=items.size()){
			return false;
		}
		return true;
	}

	@Override
	public Object next() {
		Object menuitem=items.get(position);
		position++;
		return menuitem;
	}

}
