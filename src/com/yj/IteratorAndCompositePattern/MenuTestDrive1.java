package com.yj.IteratorAndCompositePattern;

public class MenuTestDrive1 {
	
	public static void main(String[] args) {
		MenuComponent pancakeHouseMenu=new Menu1("PANCAKE HOUSE MENU", "Breakfast");
		MenuComponent dinerMenu=new Menu1("DINER MENU", "Lunch");
		MenuComponent cafeMenu=new Menu1("CAFE MENU", "Dinner");
		MenuComponent dessertMenu=new Menu1("DESSERT MENU", "Dessert of course!");
		
		MenuComponent allMenus=new Menu1("ALL MENUS", "All menus combined");
		
		allMenus.add(pancakeHouseMenu);
		allMenus.add(dinerMenu);
		allMenus.add(cafeMenu);
		
		dinerMenu.add(new MenuItem("Pasta", "Spaghetti with Marinara Sauce, and a slice of sourdough bread", true, 3.89));
		
		dinerMenu.add(dessertMenu);
		
		dessertMenu.add(new MenuItem("Apple Pie", "Apple pie with a flakey crust, topped with vanilla ice cream", true, 1.59));
		
		Waitress1 alice=new Waitress1(allMenus);
		
		//alice.printMenu();
		alice.printVegetarianMenu();
	}
}
