package com.yj.IteratorAndCompositePattern;

public interface Iterator {
	boolean hasNext();
	Object next();
}
