package com.yj.IteratorAndCompositePattern;

public class MenuTestDrive {
	
	public static void main(String[] args) {
		PancakeHouseMenu pancakeHouseMenu=new PancakeHouseMenu();
		DinerMenu dinerMenu=new DinerMenu();
		CafeMenu cafeMenu=new CafeMenu();
		
		Waitress alice=new Waitress(pancakeHouseMenu, dinerMenu,cafeMenu);
		
		alice.printMenu();
		
//		User user1=new User();
//		User user2=user1;
//		user1.name="user2";
//		System.out.println(user2.name);
	}
	
	
}

class User{
	String name="user1";
	public User(){};
}
