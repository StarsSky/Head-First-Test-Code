package com.yj.IteratorAndCompositePattern;

import java.util.ArrayList;
import java.util.Iterator;

public class Menu1 extends MenuComponent{
	ArrayList menuComponents=new ArrayList<>();
	String name;
	String description;
	
	public Menu1(String name,String description){
		this.name=name;
		this.description=description;
	}
	
	public void add(MenuComponent menuComponent){
		menuComponents.add(menuComponent);
	}
	
	public void remove(MenuComponent menuComponent){
		menuComponents.remove(menuComponent);
	}
	
	public MenuComponent getChild(int i){
		return (MenuComponent)menuComponents.get(i);
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
	public void print(){
		System.out.println("\n"+getName());
		System.out.println(", "+getDescription());
		System.out.println("----------------------");
		
		Iterator iterator=menuComponents.iterator();
		while(iterator.hasNext()){
			MenuComponent menuComponent=(MenuComponent) iterator.next();
			menuComponent.print();
		}
	}

	@Override
	public Iterator createIterator() {
		// TODO Auto-generated method stub
		return new CompositeIterator(menuComponents.iterator());
	}
}
