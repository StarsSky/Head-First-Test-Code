package com.yj.IteratorAndCompositePattern;
import java.util.Iterator;

public interface Menu {
	public Iterator createIterator();
}
