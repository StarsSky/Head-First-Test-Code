package com.yj.DecoratorPattern;

public abstract class CondimentDecorator extends Beverage {
	public abstract String getDescription();
}
