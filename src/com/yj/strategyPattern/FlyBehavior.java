package com.yj.strategyPattern;

public interface FlyBehavior {
	public void fly();
}
