package com.yj.strategyPattern;

public interface QuackBehavior {
	public void quack();
}
