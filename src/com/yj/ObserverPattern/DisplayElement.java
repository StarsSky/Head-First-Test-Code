package com.yj.ObserverPattern;

public interface DisplayElement {
	public void display();
}
