package com.yj.ObserverPattern;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class SwingObserverExample {
	JFrame frame;
	
	public static void main(String[] args) {
		SwingObserverExample example=new SwingObserverExample();
		example.go();
	}
	
	public void go(){
		frame=new JFrame();
		
		JButton button=new JButton("Should I do it?");
		button.setSize(100, 100);
		button.setPreferredSize(new Dimension(100, 100));
		button.addActionListener(new AngelListener());
		button.addActionListener(new DevilListener());
		frame.getContentPane().add(BorderLayout.CENTER, button);
		frame.setSize(300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);;
		Toolkit toolkit=Toolkit.getDefaultToolkit();
		Dimension dimension=toolkit.getScreenSize();
		double screenWidth=dimension.getWidth();
		double screenHeight=dimension.getHeight();
		frame.setLocation((int)(screenWidth-300)/2, (int)(screenHeight-300)/2);
		frame.setVisible(true);
	}
	
	class AngelListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("Don't do it, you might regret it!");
		}
	}
	
	class DevilListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("Come on, do it!");
		}
	}
}
