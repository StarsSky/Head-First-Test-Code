package com.yj.ObserverPattern;

import java.util.ArrayList;

public class WeatherData implements Subject{
	private ArrayList observers;
	private float temprature;
	private float humidity;
	private float pressure;
	
	public WeatherData(){
		observers=new ArrayList();
	}
	
	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		int i=observers.indexOf(o);
		if(i>=0){
			observers.remove(i);
		}
	}

	@Override
	public void notifyObservers() {
		for(int i=0;i< observers.size();i++){
			Observer observer=(Observer) observers.get(i);
			observer.update(temprature, humidity, pressure);
		}
	}
	
	public void measurementChanged() {
		notifyObservers();
	}
	
	public void setMeasurements(float temperature,float humidity,float pressure){
		this.temprature=temperature;
		this.humidity=humidity;
		this.pressure=pressure;
		measurementChanged();
	}
	
	//WeatherData的其他方法
	
}
