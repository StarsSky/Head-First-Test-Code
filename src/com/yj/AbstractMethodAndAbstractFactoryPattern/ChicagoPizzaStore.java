package com.yj.AbstractMethodAndAbstractFactoryPattern;

public class ChicagoPizzaStore extends PizzaStore{

	@Override
	Pizza createPizza(String type) {
		
		return new ChicagoStyleCheesePizza();
	}

}
