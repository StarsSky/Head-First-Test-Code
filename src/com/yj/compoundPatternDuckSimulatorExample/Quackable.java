package com.yj.compoundPatternDuckSimulatorExample;

public interface Quackable {
	public void quack();
}
