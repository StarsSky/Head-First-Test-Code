package com.yj.AdapterPattern;

import java.util.ArrayList;
import java.util.List;

public class EnumerationAdaterTest {
	
	public static void main(String[] args) {
		List<Integer> list=new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		EnumerationAdapter<Integer> enumeration=new EnumerationAdapter<>(list.iterator());
		while(enumeration.hasMoreElements()){
			System.out.println(enumeration.nextElement());
		}
	}
}
