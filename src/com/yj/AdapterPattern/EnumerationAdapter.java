package com.yj.AdapterPattern;

import java.util.Enumeration;
import java.util.Iterator;

public class EnumerationAdapter<T> implements Enumeration<T>{
	Iterator<T> iterator;
	
	public EnumerationAdapter(Iterator<T> iterator) {
		this.iterator=iterator;
	}

	@Override
	public boolean hasMoreElements() {
		return iterator.hasNext();
	}

	@Override
	public T nextElement() {
		// TODO Auto-generated method stub
		return iterator.next();
	}
	

}
