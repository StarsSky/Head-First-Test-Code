package com.yj.AdapterPattern;

public interface Duck {
	public void quack();
	default void fly(){
		
	};
}
