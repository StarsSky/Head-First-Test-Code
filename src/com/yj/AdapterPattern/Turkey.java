package com.yj.AdapterPattern;

public interface Turkey {
	public void gobble();
	public void fly();
}
