package com.yj.mvcPattern;
  
public interface BPMObserver {
	void updateBPM();
}
