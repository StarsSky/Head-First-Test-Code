package com.yj.mvcPattern;
  
public interface BeatObserver {
	void updateBeat();
}
