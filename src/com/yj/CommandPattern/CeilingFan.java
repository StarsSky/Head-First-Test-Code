package com.yj.CommandPattern;

public class CeilingFan {
	String useStation;
	public static final int HIGH=3;
	public static final int MEDIUM=2;
	public static final int LOW=1;
	public static final int OFF=0;
	int speed;
	
	public CeilingFan(String useStation){
		this.useStation=useStation;
		speed=OFF;
	}
	
	public void on(){
		System.out.println(useStation+" CeilingFan is on");
		low();
	}
	
	public void off(){
		System.out.println(useStation+" CeilingFan is off");
		speed=OFF;
	}
	
	public void high(){
		System.out.println(useStation+" CeilingFan run highly");
		speed=HIGH;
	}
	
	public void medium(){
		System.out.println(useStation+" CeilingFan run medium");
		speed=MEDIUM;
	}
	
	public void low(){
		System.out.println(useStation+" CeilingFan run low");
		speed=LOW;
	}
	
	public int getSpeed(){
		return speed;
	}
}
