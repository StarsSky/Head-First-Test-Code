package com.yj.CommandPattern;

public class RemoteLoader1 {
	
	public static void main(String[] args) {
		RemoteControl remoteControl=new RemoteControl();
		
		Light livingRoomLight=new Light("Living Room");
		
		LightOnCommand lightOnCommand=new LightOnCommand(livingRoomLight);
		LightOffCommand lightOffCommand=new LightOffCommand(livingRoomLight);
		
		remoteControl.setCommand(0, lightOnCommand, lightOffCommand);
		
		remoteControl.onButtonWasPushed(0);
		remoteControl.offButtonWasPushed(0);
		System.out.println(remoteControl);
		remoteControl.undoButtonWasPushed();
		remoteControl.offButtonWasPushed(0);
		remoteControl.onButtonWasPushed(0);
		System.out.println(remoteControl);
		remoteControl.undoButtonWasPushed();
		
	}
}
