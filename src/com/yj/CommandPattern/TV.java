package com.yj.CommandPattern;


public class TV {
	String location;
	
	public TV(String location) {
		this.location=location;
	}
	
	public void on(){
		System.out.println(location+" TV is on");
	}
	
	public void off(){
		System.out.println(location+" TV is off");
	}
	
	public void setInputChannel(String inputChannel){
		System.out.println(location+" TV is play "+inputChannel);
	}
	
	public void setVolume(int volume){
		System.out.println(location+" TV set volume to "+volume);
	}
}
