package com.yj.CommandPattern;

public class RemoteLoader3 {
	
	public static void main(String[] args) {
		RemoteControl remoteControl=new RemoteControl();
		
		Light light=new Light("living Room");
		TV tv=new TV("Living Room");
		Stereo stereo=new Stereo("Living Room");
		Hottub hottub=new Hottub();
		
		LightOnCommand lightOn=new LightOnCommand(light);
		StereoOnWithCDCommand stereoOnWithCDCommand=new StereoOnWithCDCommand(stereo);
		TVOnCommand tvOnCommand=new TVOnCommand(tv);
		HottubOnCommand hottubOnCommand=new HottubOnCommand(hottub);
		
		LightOffCommand lightOffCommand=new LightOffCommand(light);
		StereoOffCommand stereoOffCommand=new StereoOffCommand(stereo);
		TVOffCommand tvOffCommand=new TVOffCommand(tv);
		HottubOffCommand hottubOffCommand=new HottubOffCommand(hottub);
		
		Command[] partyOn={lightOn,stereoOnWithCDCommand,tvOnCommand , hottubOnCommand};
		Command[] partyOff={lightOffCommand,stereoOffCommand,tvOffCommand,hottubOffCommand};
		
		MacroCommand partyOnMacro=new MacroCommand(partyOn);
		MacroCommand partyOffMacro=new  MacroCommand(partyOff);
		
		remoteControl.setCommand(0, partyOnMacro, partyOffMacro);
		
		System.out.println(remoteControl);
		System.out.println("--- Pushing Macro On---");
		remoteControl.onButtonWasPushed(0);
		System.out.println("--- Pushing Macro Off---");
		remoteControl.offButtonWasPushed(0);
		System.out.println("--- Pushing undo---");
		remoteControl.undoButtonWasPushed();
	}
}
