package com.yj.CommandPattern;

public class RemoteControlTest {
	
	public static void main(String[] args) {
		SimpleRemoteControl remote=new SimpleRemoteControl();
		Light light=new Light("");
		LightOnCommand lightOn=new LightOnCommand(light);
		
		remote.setCommand(lightOn);
		remote.buttonWasPressed();
		
		GarageDoor garageDoor=new GarageDoor();
		GarageDoorOpenCommand garageDoorOpenCommand=new GarageDoorOpenCommand(garageDoor);
		
		remote.setCommand(garageDoorOpenCommand);
		remote.buttonWasPressed();
	}
}
