package com.yj.CommandPattern;

public class Hottub {
	
	public void on(){
		System.out.println("hottub is on");
	}
	
	public void off(){
		System.out.println("hotthub is off");
	}
	
	public void circulate(){
		System.out.println("hotthub is circulating");
	}
	
	public void jetsOn(){
		System.out.println("hottub jets is on");
	}
	
	public void jetsOff(){
		System.out.println("hottub jets is off");
	}
	
	public void setTemperature(int temperature){
		System.out.println("hottub set temperature to "+temperature);
	}
}
