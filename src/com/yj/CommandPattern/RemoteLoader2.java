package com.yj.CommandPattern;

public class RemoteLoader2 {
	
	public static void main(String[] args) {
		RemoteControl remoteControl=new RemoteControl();
		
		CeilingFan ceilingFan=new CeilingFan("Living Room");
		
		CeilingFanOnCommand ceilingFanOnCommand=new CeilingFanOnCommand(ceilingFan);
		CeilingFanHighCommand ceilingFanHighCommand=new CeilingFanHighCommand(ceilingFan);
		CeilingFanOffCommand ceilingFanOffCommand=new CeilingFanOffCommand(ceilingFan);
		
		remoteControl.setCommand(0, ceilingFanOnCommand, ceilingFanOffCommand);
		remoteControl.setCommand(1, ceilingFanHighCommand, ceilingFanOffCommand);
		
		remoteControl.onButtonWasPushed(0);
		remoteControl.offButtonWasPushed(0);
		System.out.println(remoteControl);
		remoteControl.undoButtonWasPushed();
		
		remoteControl.onButtonWasPushed(1);
		System.out.println(remoteControl);
		remoteControl.undoButtonWasPushed();
	}
}
