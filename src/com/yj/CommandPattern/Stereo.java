package com.yj.CommandPattern;

public class Stereo {
	String useStation;
	
	public Stereo(String useStation){
		this.useStation=useStation;
	}
	
	public void on(){
		System.out.println(useStation+" Stereo is on");
	}
	
	public void off(){
		System.out.println(useStation+" Stereo is off");
	}
	
	public void setCd(){
		System.out.println(useStation+" Stereo is setting CD");
	}
	
	public void setDvd(){
		System.out.println(useStation+" Stereo is setting DVD");
	}
	
	public void setRadio(){
		System.out.println(useStation+" Stereo is setting Radio");
	}
	
	public void setVolume(int volume){
		System.out.println(useStation+" Stereo set Volume to "+volume);
	}
}
