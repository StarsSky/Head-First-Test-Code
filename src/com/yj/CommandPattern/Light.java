package com.yj.CommandPattern;

public class Light {
	String lightName;
	
	public Light(String name){
		this.lightName=name;
	}
	
	public void on(){
		System.out.println(lightName+" Light is On");
	}
	
	public void off(){
		System.out.println(lightName+" Light is off");
	}
}
